# test

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### Design

Styles are fully isolated by vue's scoped module system and shadowRoot;

### How to connect the widget to the page

In app where we want to add a widget, we should add a script tag to the bottom of the page with build results, which placed in /dist/feedbacks-widget. Then just add a <feedbacks-widget> to the HTML and that's all. For a live example, please move to the demo folder in the root of this project and see how it connects to the existing app via web-component widget. You can also check that the styles of the created widget are encapsulated.
